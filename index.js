const http = require("http");

const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url === "/greeting") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to the login page");
	/*
		Practice - create two more uri's and let your users see the message "Welcome to _____ Page"; use successful status code and plain text as the content
	*/
	} else if (request.url === "/home") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("This is Home Page");
	}
	else if (request.url === "/contact") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("This is Contact Page");
	}
	else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});

server.listen(port);

console.log(`Welcome to the login page ${port}`);
